#!bin/bash

Debug="unassigned"
counter="0"
folder="log"
time_date=$(date)
sensor_output="unassigned"
IFS="	"
time_date=$(date)

while read tag status; do 
	if [ "$tag" == "Debuging" ]; then
		Debug="$status"
	fi
done < "paras.txt"

while read a; do
	results_file="$a.txt"
done < "$folder/results_file_add.txt"

while read name ros_command topic; do

unset IFS

	if [ "$name" != "name" ]; then
		node="$ros_command"
	
		(nohup rosrun $node &>/dev/null &)
		sleep 1
		while [ "$sensor_output" != "Recieved Message" ]; do
			source ~/patrick_ws/devel/setup.bash
			sensor_output=$(rosrun $(echo "tf_tree Subscriber _topic:=$topic"))
			counter=$((counter+1))
			if [ "$counter" -gt "5" ]; then
				(rosnode kill $node >/dev/null)
				if [ "$Debug" == "Unsuppressed" ]; then
					echo "Communication with sensor failed, retrying...."
				fi
				. echo.sh "Sensor Check"
				return
			fi
		done
		rosnode kill $node >/dev/null

		if [ "$Debug" == "Unsuppressed" ]; then
			echo "Communication with sensor successful"
			echo "Communication with sensor successful	$time_date" >> "$folder/$results_file"
		fi

	fi
IFS="	"
done < "list_of_sensors.txt"
unset IFS
