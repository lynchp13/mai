#!/bin/bash
source patrick_ws/devel/setup.bash
export ROS_HOSTNAME=tcdrobotics3
export ROS_IP=tcdrobotics3
export ROS_MASTER_URI=http://tcdrobotics3:11311

echo "ROS_HOSTNAME" $ROS_HOSTNAME
echo "ROS_IP:" $ROS_IP
echo "ROS_MASTER_URI:" $ROS_MASTER_URI
