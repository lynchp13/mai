#!bin/bash

index="0"
Debug="unassigned"

folder="log"
time_date=$(date)

while read tag status; do 
	if [ "$tag" == "Debuging" ]; then
		Debug="$status"
	fi
done < "paras.txt"

while read a; do
	results_file="$a.txt"
done < "$folder/results_file_add.txt"

while read username ip_address; do 

	if [ "$username" != "$USER" ]; then

		. pinging.sh "$ip_address"

		while read tag status t_d; do
			if [ "$tag" == "pinging_$ip_address:" ]; then
				success_failure="$status"
			fi
		done < "$folder/$results_file"
		if [ "$success_failure" == "successful" ]; then
			if [ "$Debug" == "Unsuppressed" ]; then
				echo "Ping $ip_address successful"
			fi
		else 
			. echo.sh "Ping other computers"
		fi
	fi
done < "computers.txt" 
