#!bin/bash

folder="log"
time_date=$(date)
ip_add="unassigned"
counter="0"

while read tag status; do 
	if [ "$tag" == "Debuging" ]; then
		Debug="$status"
	fi
done < "paras.txt"

while read a; do
	results_file="$a.txt"
done < "$folder/results_file_add.txt"


while read tag status t_d; do
	if [ "$tag" == "IP_address:" ] && [ "$counter" == "0" ]; then
		ip_add="$status"
		counter=$((counter+1))
	fi
done < "$folder/$results_file"

if [ "$ip_add" == "unassigned" ]; then
	echo "$(tput setaf 1)Cannot ping, have not aquired IP address yet.  Check order of "to-do.txt" file $(tput sgr 0)"
	echo "$(tput setaf 1)Aborting Launch $(tput sgr 0)"
	echo "ABORTING	ABORTING	ABORTING" >> "$folder/$results_file"
	return
fi

. pinging.sh "$ip_add"

while read tag status t_d; do
	if [ "$tag" == "pinging_$ip_add:" ]; then
		success_failure="$status"
	fi
done < "$folder/$results_file"

if [ "$success_failure" == "successful" ]; then
	if [ "$Debug" == "Unsuppressed" ]; then
		echo "Ping myself successful"
	fi
else
	. echo "Pinging myself"
fi