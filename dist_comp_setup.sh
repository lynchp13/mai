#!bin/bash

g_pre="initilise"
g_acc_offset="initilise"
index="0"
Debug="unassigned"

folder="log"
time_date=$(date)

while read tag status; do 
	if [ "$tag" == "Debuging" ]; then
		Debug="$status"
	fi
	if [ "$tag" == "Offset_Acceptable(secs)" ]; then
		g_acc_offset="$status"
	fi
done < "paras.txt"

while read a; do
	results_file="$a.txt"
done < "$folder/results_file_add.txt"

while read username ip_address; do 

	if [ "$username" != "$USER" ]; then

		if [ "$Debug" == "Unsuppressed" ]; then
		echo "*********** Start $ip_address setup **************"
		fi

		. pinging.sh "$ip_address"

		while read tag status t_d; do
			if [ "$tag" == "pinging_$ip_address:" ]; then
				success_failure="$status"
			fi
		done < "$folder/$results_file"
		if [ "$success_failure" == "successful" ]; then
			if [ "$Debug" == "Unsuppressed" ]; then
				echo "Ping $ip_address successful"
			fi
		else 
			. echo.sh "Ping other computers"
		fi
	fi








#		packet_loss=$(ping -c 3 -q -w 3 "$b")
#		packet_loss=($packet_loss)
#		counter="0"
#		for i in "${packet_loss[@]}"
#		do
#			counter=$((counter+1))
#			if [ "$i" == "packet" ] ; then	
#				index=$((counter-2))	
#			fi
#		done
#		percentage_loss=${packet_loss[$index]}
#	
#		if [ "$percentage_loss" == "0%" ]; then
#			echo "ping sucessful"
#			echo "writing results to "$folder/$results_file""
#			echo "ping to $b:	sucessful	$time_date" >> "$folder/$results_file"	
#
#			ssh "$a@$b" ". ros_setup.sh"
#			echo "writing results to "$folder/$results_file""
#			echo "Established Master:	$this_computer_IP	$time_date" >> "$folder/$results_file"
#
#			offset_sec=$(ssh "$a@$b" "ntpdate -d $b | sed -n '$ s/.*offset //p'")
#			echo "Clock offset between $a and $USER: $offset_sec"
#			echo "writing results to "$folder/$results_file""		
#			echo "Clock offset ($a to master):	$offset_sec	$time_date" >> "$folder/$results_file"		
#			offset=($offset_sec)
#	
#			if [ $offset \< 0 ]; then	
#				temp="-1"
#				offset=$(echo "scale=8; $offset*$temp" | bc)
#			fi
#		
#			if [ $(echo $offset'>'$g_acc_offset | bc -l) -eq 1 ]; then
#				echo "$(tput setaf 3)Clock offset too big.  Install Chrony and establish $USER as a server$(tput sgr 0)"
#				echo "writing results to "$folder/$results_file""		
#				echo "Clock offset ($a to master):	unacceptable	$time_date" >> "$folder/$results_file"
#			else
#				echo "Clock offset acceptable.  Continue....."
#				echo "writing results to "$folder/$results_file""		
#				echo "Clock offset ($a to master):	acceptable	$time_date" >> "$folder/$results_file"
#			fi
#			echo "attempting to activate weapons systems....."
#			sleep 1 
#			echo "activation failed"
#
#		else
#			echo "ping unsucessful. Network problem"
#			echo "writing results to "$folder/$results_file""
#			echo "ping to $b:	unsucessful	$time_date" >> "$folder/$results_file"
#			echo "$(tput setaf 3)Unable to communicate with $b$(tput sgr 0)"
#		fi
#		echo "************** End $b setup ****************"
#	
#	fi	
#
done < "computers.txt" 
#
#echo "************** Launching ROS core on $USER ****************"
#nohup gnome-terminal -e roscore </dev/null &>/dev/null &
#. monitor.sh &
#wait
