#!bin/bash

Debug="unassigned"

while read tag status; do 
	if [ "$tag" == "Debuging" ]; then
		Debug="$status"
	fi
done < "paras.txt"

index="unassigned"
failed_counter="0"
success_counter="0"
sleep 5
unset IFS
while [ 1 ]
do
	output=$(rostopic list)
	output=($output)	
	counter="0"
	for i in "${output[@]}"
	do
		counter=$((counter+1))
		if [ "$i" == "/rosout" ] ; then	
			index=$((counter))	
		fi
	done
	
	if [ "$index" == "unassigned" ]; then 
		failed_counter=$(($failed_counter+1))	
		success_counter="0"
	else
		success_counter=$((success_counter+1))
		failed_counter="0"
		counter="0"
		if [ "$success_counter" -gt "3" ]; then
			if [ "$Debug" == "Unsuppressed" ]; then
				echo "ROS core launch successful"
			fi
			return
		fi
	fi
	index="unassigned"
	counter="0"
	if [ "$failed_counter" -gt "3" ]; then
		echo "$(tput setaf 1)Cannot communicate with ROS core, Aborting Launch$(tput sgr 0)"
		return
	fi

done 