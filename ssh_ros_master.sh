#!/bin/bash
Debug="unassigned"
folder="log"
time_date=$(date)

while read tag status; do 
	if [ "$tag" == "Debuging" ]; then
		Debug="$status"
	fi
done < "paras.txt"

while read a; do
	results_file="$a.txt"
done < "$folder/results_file_add.txt"

export ROS_HOSTNAME=tcdrobotics3
export ROS_IP=tcdrobotics3
export ROS_MASTER_URI=http://tcdrobotics3:11311

if [ "$Debug" == "Unsuppressed" ]; then
	echo "ROS_HOSTNAME" $ROS_HOSTNAME
	echo "ROS_IP:" $ROS_IP
	echo "ROS_MASTER_URI:" $ROS_MASTER_URI
	echo "Writing results to file"
fi

echo "ROS_HOSTNAME" $ROS_HOSTNAME "		$time_date">> "$folder/$results_file"
echo "ROS_IP:" $ROS_IP "	$time_date" >> "$folder/$results_file"
echo "ROS_MASTER_URI:" $ROS_MASTER_URI "	$time_date" >> "$folder/$results_file"