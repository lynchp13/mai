### What is this repository for? ###

This repo contains a robot startup procedure completed for part of my MAI project.

The launch procedure contains a framework for the implimentation of a robot launch procedure aswell as some basic tests and checks useful during robot launch. This framework is easily expandable and includes logging, error messages and recovery script tools

### How do I get set up? ###

- Clone
- Follow instructions in the comments of each script in order to use that test/check.
- modify "to-do.txt" file to change what happens during the launch 
- launch by running start_launch.sh
- When satisfied run start_launch on boot of each computer on the robot.