#!bin/bash
echo "$(date)"
counter="0"
index="unassigned"
folder="log"
time_date=$(date)
results_file="results $time_date.txt"

if [ ! -d "$folder" ]; then
	mkdir "$folder"
fi
pos_of_results="results $time_date"
echo "$pos_of_results" > "$folder/results_file_add.txt"
echo "BEGINING" > "$folder/$pos_of_results.txt"
IFS="	"
while read vtype tag Priority Recovery_Script Message Test_Script; do 

	unset IFS

	if [ "$vtype" != "Type" ]; then

		echo "$(tput setaf 2)$Test_Script$(tput sgr 0)"
		. "$Test_Script"

		while read tag status t_d; do
			if [ "$tag" == "ABORTING" ]; then
				return
			fi
		done < "$folder/$pos_of_results.txt"
		#echo "$(date)"
	fi

	IFS="	"

done < "/home/$USER/mai/to_do.txt"

echo "$(tput setaf 2)Successfully Completed Launch$(tput sgr 0)"
