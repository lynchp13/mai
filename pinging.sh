ping_address=$1
#!bin/bash
first="sf"
index="unassigned"
folder="log"
Debug="unassigned"
time_date=$(date)


while read tag status; do 
	if [ "$tag" == "Debuging" ]; then
		Debug="$status"
	fi
done < "paras.txt"

while read a; do
	results_file="$a.txt"
done < "$folder/results_file_add.txt"

if [ "$Debug" == "Unsuppressed" ]; then
	echo "Pinging $ping_address"
fi

ping_responce=$(ping -c 3 -q -w 3 $ping_address)
ping_responce=($ping_responce)
counter="0"
for l in "${ping_responce[@]}"
do			
	counter=$((counter+1))
	if [ "$l" == "packet" ] ; then
		index=$((counter-2))	
	fi
done

	percentage_loss=${ping_responce[$index]}
if [ "$percentage_loss" == "0%" ]; then
	if [ "$Debug" == "Unsuppressed" ]; then
		echo "ping to $ping_address sucecssful"
		echo "writing results to "$folder/$results_file""
	fi
	echo "pinging_$ping_address:	successful	$time_date" >> "$folder/$results_file"
else
	if [ "$Debug" == "Unsuppressed" ]; then
		echo "ping to $ping_address unsuccessful"
		echo "writing results to "$folder/$results_file""
	fi
		echo "pinging_$ping_address:	unsuccessful	$time_date" >> "$folder/$results_file"
fi
sleep 1