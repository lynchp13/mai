#!/usr/bin/python

import roboclaw
import sys
#Linux comport name
port = '/dev/' + str(sys.argv[1])
try:
	roboclaw.Open(port, 115200)
except:
	print("  Motor driver not found")
	sys.exit()

error = roboclaw.ReadError(0x80)
if error[1] == 0:
	print("Communication with " + port + " successful") 

num = 0x00
num = error[1]
num = num >> 6
if num&0x01:
	print("1")
if num&0x02:
	print("2")
if num&0x04:
	print("4")
if num&0x08:
	print("8")
if num&0x10:
	print("10")
if num&0x20:
	print("20")
if num&0x40:
	print("40")
if num&0x80:
	print("80")
