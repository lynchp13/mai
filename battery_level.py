#!/usr/bin/python

import roboclaw
import sys
#Windows comport name
#roboclaw.Open("COM3",115200)
#Linux comport name
port = '/dev/' + str(sys.argv[1])
try:
	roboclaw.Open(port, 115200)
except:
	print("  Motor driver not found")
	sys.exit()

battery_level = roboclaw.ReadMainBatteryVoltage(0x80)
print(battery_level[1])