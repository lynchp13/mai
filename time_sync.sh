#!bin/bash
g_acc_offset="initilise"
index="0"
Debug="unassigned"

folder="log"
time_date=$(date)

while read tag status; do 
	if [ "$tag" == "Debuging" ]; then
		Debug="$status"
	fi
	if [ "$tag" == "Offset_Acceptable(secs)" ]; then
		g_acc_offset="$status"
	fi
done < "paras.txt"

while read a; do
	results_file="$a.txt"
done < "$folder/results_file_add.txt"

while read username ip_address; do 

	if [ "$username" != "$USER" ]; then

		if [ "$Debug" == "Unsuppressed" ]; then
		echo "Checking time offset between $username and $USER"
		fi
		offset_sec=$(ssh "$username@$ip_address" "ntpdate -d $ip_address | sed -n '$ s/.*offset //p'")
		if [ "$Debug" == "Unsuppressed" ]; then
		echo "Clock offset between $a and $USER: $offset_sec"
		echo "writing results to "$folder/$results_file""
		fi
		
		echo "Clock offset ($a to master):	$offset_sec	$time_date" >> "$folder/$results_file"	
		unset IFS	
		offset=($offset_sec)
		if [ ${offset[0]} \< 0 ]; then	
				temp="-1"
				offset=$(echo "scale=8; $offset*$temp" | bc)
		fi
		if [ "$(echo $offset'>'$g_acc_offset | bc -l)" -eq "1" ]; then
			echo "Clock offset ($a to master):	unacceptable	$time_date" >> "$folder/$results_file"
			. echo.sh "Time sync"
			if [ "$Debug" == "Unsuppressed" ]; then
				echo "writing results to "$folder/$results_file""
			fi
		else
			if [ "$Debug" == "Unsuppressed" ]; then
				echo "Clock offset acceptable.  Continue....."
				echo "writing results to "$folder/$results_file""		
			fi			
			echo "Clock offset ($a to master):	acceptable	$time_date" >> "$folder/$results_file"
		fi
#			echo "attempting to activate weapons systems....."
#			sleep 1 
#			echo "activation failed"
	fi
done < "computers.txt" 