#!bin/bash
error=$1
unset IFS
more_detail=$2
Error_status="unassigned"
Warning_status="unassigned"
Debug_status="unassigned"
s_f="unassigned"
unset IFS
while read a; do
	results_file="$a.txt"
done < "$folder/results_file_add.txt"

while read label status; do
	if [ "$label" == "Errors" ]; then
		Error_status=$status
	fi
	if [ "$label" == "Warnings" ]; then
		Warning_status=$status
	fi
	if [ "$label" == "Debuging" ]; then
		Debug_status=$status
	fi
done < paras.txt

if [ "$Error_status" != "Suppressed" ] && [ "$Error_status" != "Unsuppressed" ]; then
	echo "Invalid Parameter in paras.txt file (Suppressed/Unsuppressed)"
fi
if [ "$Warning_status" != "Suppressed" ] && [ "$Warning_status" != "Unsuppressed" ]; then
	echo "Invalid Parameter in paras.txt file (Suppressed/Unsuppressed)"
fi
if [ "$Debug_status" != "Suppressed" ] && [ "$Debug_status" != "Unsuppressed" ]; then
	echo "Invalid Parameter in paras.txt file (Suppressed/Unsuppressed)"
fi

IFS='	'
while read category tag priority recovery message test_script; do

if [ "$tag" == "$error" ]; then

	if [ "$priority" == "High" ]; then

		if [ "$category" == "ERR" ] && [ "$Error_status" == "Unsuppressed" ]; then
			echo "$(tput setaf 1)[$category][$tag]$message $more_detail$(tput sgr 0)"
		elif [ "$category" == "WARN" ] && [ "$Warning_status" == "Unsuppressed" ]; then
			echo "$(tput setaf 3)[$category][$tag]$message $more_detail$(tput sgr 0)"
		elif [ "$category" == "Degug" ] && [ "$Debug_status" == "Unsuppressed" ]; then
			echo "[$category][$tag]$message $more_detail"
		else
			:
		fi
		echo "$(tput setaf 1)[$category][$tag]Aborting Launch $(tput sgr 0)"
		echo "ABORTING" >> "$folder/$results_file"
		return
	elif [ "$priority" == "Medium" ]; then

		if  [ "$category" == "ERR" ] && [ "$Error_status" == "Unsuppressed" ]; then
			echo "$(tput setaf 1)[$category][$tag]$message $more_detail$(tput sgr 0)"
		elif [ "$category" == "WARN" ] && [ "$Warning_status" == "Unsuppressed" ]; then
			echo "$(tput setaf 3)[$category][$tag]$message $more_detail$(tput sgr 0)"
		elif [ "$category" == "Degug" ] && [ "$Debug_status" == "Unsuppressed" ]; then
			echo "[$category][$tag]$message $more_detail"
		else
			:
		fi
	
		if [ "$recovery" == "NA" ]; then
			if [ "$category" == "ERR" ] && [ "$Error_status" == "Unsuppressed" ]; then						
				echo "$(tput setaf 1)[$category][$tag]No recovery behaviour specified $(tput sgr 0)"
				echo "$(tput setaf 3)Priority Elevated to High$(tput sgr 0)"
				echo "$(tput setaf 1)Aborting Launch$(tput sgr 0)"
				echo "ABORTING	ABORTING	ABORTING" >> "$folder/$results_file"
			elif [ "$category" == "WARN" ] && [ "$Warning_status" == "Unsuppressed" ]; then
				echo "$(tput setaf 3)[$category][$tag]No recovery behaviour specified $(tput sgr 0)"
				echo "$(tput setaf 3)Priority Elevated to High$(tput sgr 0)"
				echo "$(tput setaf 1)Aborting Launch$(tput sgr 0)"
				echo "ABORTING	ABORTING	ABORTING" >> "$folder/$results_file"
			elif [ "$category" == "Debug" ] && [ "$Debug_status" == "Unsuppressed" ]; then		
				echo "[$category][$tag]No recovery behaviour specified"
				echo "$(tput setaf 3)Priority Elevated to High$(tput sgr 0)"
				echo "$(tput setaf 1)Aborting Launch$(tput sgr 0)"
				echo "ABORTING	ABORTING	ABORTING" >> "$folder/$results_file"
			else 
				:
			fi
		else
			echo "$(tput setaf 3)[$category][$tag]Attempting to resolve the problem $(tput sgr 0)"
			echo "$(tput setaf 3)[$category][$tag]Running $recovery$(tput sgr 0)"
			status=$(. $recovery)
			IFS=$'\n'
			status=($status)
			echo "${status[0]}"
			unset IFS
			for i in "${status[@]}"
			do	
				if [ "$i" == "Recovery Successful" ] ; then
				s_f="successful"
				fi
			done
			if [ "$s_f" == "successful" ]; then
				echo "$(tput setaf 2)Success continuing with launch$(tput sgr 0)"
			else
				echo "$(tput setaf 3)Recovery Failed$(tput sgr 0)"
				echo "$(tput setaf 3)Priority Elevated to High$(tput sgr 0)"
				echo "$(tput setaf 1)Aborting Launch$(tput sgr 0)"
				echo "ABORTING	ABORTING	ABORTING" >> "$folder/$results_file"
			fi
		fi


	elif [ "$priority" == "Low" ]; then
		if [ "$category" == "ERR" ] && [ "$Error_status" == "Unsuppressed" ]; then
			echo "$(tput setaf 1)[$category][$tag]$message $more_detail$(tput sgr 0)"
		elif [ "$category" == "WARN" ] && [ "$Warning_status" == "Unsuppressed" ]; then
			echo "$(tput setaf 3)[$category][$tag]$message $more_detail$(tput sgr 0)"
		elif [ "$category" == "Degug" ] && [ "$Debug_status" == "Unsuppressed" ]; then
			echo "[$category][$tag]$message $more_detail"
		else
			:
		fi

		

	elif [ "$priority" == "Priority" ]; then	
		:	

	else
		echo "Invalid priority level '$priority' in the $tag row"
	fi
	
fi
done < to_do.txt 
unset IFS