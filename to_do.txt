Type	TAG							Priority	Recovery Script			Message							Test Script
ERR		Ethernet Network Access		Low			temp_rec.sh				Not connected by Ethernet		Ethernet_check.sh
ERR		Wifi Network Access			Low			NA						Not connected by Wifi			Wifi_check.sh
ERR		Wifi Network Access			Low			NA						Unable to ping myself 			ping_me.sh
WARN 	Internet Access				Low			NA						No Internet Access 				internet_access.sh
ERR		Ping other computers 		Medium		NA						Cannot ping device on Network	ping_other_comp.sh
WARN	Time sync					Low			time_sync_recovery.sh	Time offset unacceptable		time_sync.sh	
WARN 	Establish ROSmaster			Medium		NA						Could not establish ros master 	ssh_ros_master.sh
WARN	Low Level Hardware			Low			NA						Problem with low level Hardware	low_level_hw_check.sh
END OF FILE