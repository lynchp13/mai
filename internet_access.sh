#!bin/bash

folder="log"
Debug="unassigned"

while read tag status; do 
	if [ "$tag" == "Debuging" ]; then
		Debug="$status"
	fi
done < "paras.txt"

while read a; do
	results_file="$a.txt"
done < "$folder/results_file_add.txt"

ip_address="8.8.8.8"

. pinging.sh "$ip_address"

while read tag status t_d; do
	if [ "$tag" == "pinging_$ip_address:" ]; then
		success_failure="$status"
	fi
done < "$folder/$results_file"

if [ "$success_failure" == "successful" ]; then
	if [ "$Debug" == "Unsuppressed" ]; then
		echo "Ping google.com successful"
	fi
else
	. echo.sh "Internet Access"	
fi