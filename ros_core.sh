#!bin/bash
time_date=$(date)
folder="log"
while read tag status; do 
	if [ "$tag" == "Debuging" ]; then
		Debug="$status"
	fi
done < "paras.txt"

while read a; do
	results_file="$a.txt"
done < "$folder/results_file_add.txt"

echo "************** Launching ROS core on $USER ****************"
echo "************** Launching ROS core on $USER **************** $time_date" >> "$folder/$results_file"
(nohup roscore &>/dev/null &)
. monitor.sh
#. sensor_check.sh &
#terminator -e rosrun template_sm ros_con_sm.py &