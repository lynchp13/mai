#!bin/bash

counter="0"
index="unassigned"
folder="log"
Debug="unassigned"

while read tag status; do 
	if [ "$tag" == "Debuging" ]; then
		Debug="$status"
	fi
done < "paras.txt"

while read a; do
	results_file="$a.txt"
done < "$folder/results_file_add.txt"

ip_address=$(ifconfig wlp2s0)

ip_address_arr=($ip_address)

for i in "${ip_address_arr[@]}"
do
counter=$(($counter+1))
if [ "$i" == "inet" ]; then
	index="$counter"
fi
done

if [ "$index" == "unassigned" ]; then

	. echo.sh "Wifi Network Access"

else
	ip_address="${ip_address_arr[index]}"
	ip_address_arr=$(echo $ip_address | tr ":", '\n')
	ip_address_arr=($ip_address_arr)
	ip_address=${ip_address_arr[1]}

	if [ "$Debug" == "Unsuppressed" ]; then
	echo "Sucessfully aquired IP address via Wifi: $ip_address"
	echo "Writing Results to file"
	echo "Connection:	Wifi	$time_date" >> "$folder/$results_file"
	echo "IP_address:	$ip_address	$time_date" >> "$folder/$results_file"
	fi
fi


