#!bin/bash

Debug="unassigned"
message="unassigned"
folder="log"
time_date=$(date)

while read tag status; do 
	if [ "$tag" == "Debuging" ]; then
		Debug="$status"
	fi
done < "paras.txt"

while read a; do
	results_file="$a.txt"
done < "$folder/results_file_add.txt"

IFS="	"
while read name serial_port; do
unset IFS
	if [ "$name" != "Name" ]; then
		status=$(python roboclaw_error_check.py "$serial_port")
		if [ "$status" == "  Motor driver not found" ]; then
			. echo.sh "Low Level Hardware" "$status"
		elif [ "$status" == "Communication with /dev/$serial_port successful" ] && [ "$Debug" == "Unsuppressed" ]; then
			echo "$status"
			echo "$status	$time_date" >> "$folder/$results_file"
		else
			status=($status)
			for i in "${status[@]}"
			do		
				if [ "$i" == "1" ]; then
					message=" M1 OverCurrent"
				elif [ "$i" == "2" ]; then
					if [ "$message" != "unassigned" ]; then
						message="$message & M2 OverCurrent Warning"
					else
						message="M2 OverCurrent Warning"
					fi
				elif [ "$i" == "4" ]; then
					if [ "$message" != "unassigned" ]; then
						message="$message & E-Stop Warning"
					else
						message="E-Stop Warning"
					fi
				elif [ "$i" == "8" ]; then
					if [ "$message" != "unassigned" ]; then
						message="$message & Temperature Warning"
					else
						message="Temperature Warning"
					fi
				elif [ "$i" == "10" ]; then
					if [ "$message" != "unassigned" ]; then
						message="$message & Main Battery High Warning"
					else
						message="Main Battery High Warning"
					fi
				elif [ "$i" == "20" ]; then
					if [ "$message" != "unassigned" ]; then
						message="$message & Main Battery Low Warning"
					else 
						message="Main Battery Low Warning"
					fi
				elif [ "$i" == "40" ]; then
					if [ "$message" != "unassigned" ]; then
						message="$message & Logic Battery High Warning"
					else
						message="Logic Battery High Warning"
					fi
				elif [ "$i" == "80" ]; then
					if [ "$message" != "unassigned" ]; then
						message="$message & Logic Battery Low Warning"	
					else
						message="Logic Battery Low Warning"
					fi
				fi
			done
			if [ "$message" != "unassigned" ]; then
				. echo.sh "Low Level Hardware" "$message"
			fi
		fi
	fi
IFS="	"
done < "low_level_hw.txt"