#!bin/bash
Debug="unassigned"
message="unassigned"
folder="log"
time_date=$(date)

while read tag status; do 
	if [ "$tag" == "Debuging" ]; then
		Debug="$status"
	fi
done < "paras.txt"

while read a; do
	results_file="$a.txt"
done < "$folder/results_file_add.txt"

IFS="	"
while read name serial_port; do
unset IFS
	if [ "$name" != "Name" ]; then
		voltage=$(python battery_level.py "$serial_port") 

		if [ "$voltage" == "  Motor driver not found" ]; then
			echo "$voltage"
			return
		elif [ "$Debug" == "Unsuppressed" ]; then
			voltage=$(echo "scale=1; $voltage/10" | bc)
			echo "Battery Level: $voltage V"
			echo "Battery Level: $voltage V $time_date" >> "$folder/$results_file"
		fi
	fi
IFS="	"
done < "low_level_hw.txt"